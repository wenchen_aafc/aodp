#!/usr/bin/env perl
use strict;
use File::Copy;
use File::Basename;
use Cwd;
use Bio::SeqIO;
use Bio::TreeIO;
use List::MoreUtils qw/uniq/;
use File::Find;
use File::Path;
use FindBin;
use Getopt::Long;
use File::Spec;

#Modules
use lib "$FindBin::Bin/modules";
use File_Convert;
use Tree;
use Sigoli_Func;
use Output_Cleanup;
use LogFile;

$| =1; #turns off buffer so prints go in the right order
# Program Name:  Automated Oligonucleotide Design Pipeline (AODP)
# Program Purpose: Facilitate the interpretation of NGS data at species or even lower taxonomic levels by utilizing signature oligos.
# Version 1.3

# authors: Kaisheng (Kevin) Chen, Chuyang (Charles) Qi, Eric Hardy, Wen Chen* (wen.chen@agr.gc.ca)

#MIT License
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Sigoli: SigOli is now distributed under the GNU General Public License (GPL).

#DOI: 10.5281/zenodo.10762

# Needed to run this script:
#  -Perl ~3.8 or above (from Bioperl)
#  -Bioperl
#  -installed sigoli program
#  -proper modules in the folder (Tree.pm, File_Convert, Sigoli_Func)

# Script Description:
#  Takes one aligned fasta file, one .tre file, and an outgroup list file. The oligo maximum and minimum lengths need to be specified.
# Input must be in the indata directory
#  The program will output the resutls in indata/filename.date folder. It will set up folder enviornment needed for Sigoli to run.
#  Tree_and_nodes (Sigoli requirement) and a backup will be created in indata/filename.date. These are required for sigoli to run. The tree_and_nodes folder can be deleted. 
#  The sigoli for each lengths will have output in the sigoli_folder.
#  The final sigoli output for len25 and all lengths will be in the modified data folder 

#Edit the oligo min and max and thread counts.
#===============================================================================================================================================================================================

our $thread= 1;                                                                          #how many threads to use. (Generally, for a system 1 core = 1 thread. Putting more threads than your CPU core count results in no preformance gain)

our $oligo_length_min = 25;                                                             #minimum length of the oligo to be designed, e.g 10, 18, 25 etc, 

our $oligo_length_max = 28;                                                                #maximum length of the oligo to be designed, e.g. 30, 35 etc.

my $erase_tree = 1;  #determines whether the tree_and_nodes folder should be removed after the program finishes. 0 = no, 1 = yes

my $erase_sigdata = 1; #determines whether to remove sigoli's vanilla outputs, which is largely not needed after final output is generated. 0= no, 1= yes

our $erase_blastresults =1; #determines whether to remove blast results after the cross validation is finished. 0= no, 1= yes

my $erase_OFP = 0;

our $output_specific_len = 0; #determines whether or not to kill basis length stuff

#END-OF-EDITS=============================================================================================================================================================================================================================================================
#Do not make any changes below unless you are reasonably comfortable with Perl

my $filename="";                                                                                                         #input(fasta) file (ALIGNED)
my $in_tree="";                                                                                      #user's tree input file. This should end in .tre
our $s_tree;
our  $outgroup="";          #outgroup list file. This should end in .outgroup
our $isolation = 0; #-i option. User will need to include a file containing patterns to isolate from the data.
#0 = no, 1 = yes

our $isofile="";#-i option file. Initialized to blank in case nothing was found or -i is not being used.

our $nameswitch = 0; #names option. User will have to input a .names file.

our $namesfile="";

my $OFP_link =0;

our $unite = 0; #cross referencing with unite database option. 
our $unite_pattern="meow";

#file specifics. They are set in the prep subroutine
my $full_filename;                                                                                                     #full file name e.g dog.txt
my $f_extension;                                                                                                   #input file's extension e.g txt
our $s_filename;                                                                                      #small filename without the extension eg. dog
our $s_outgroup;

my $indir="$FindBin::Bin/indata";
my $config_path= "$FindBin::Bin/config";
our $folder;#folder that stores the output
our $output_dir = "$FindBin::Bin/outdata";

#files from the reference sets
our $ref_fasta;
our $ref_tax;
#data for logfile
my $logfile;
my $input_hash = {};
my @runtime_array;
our $runtime_hash= {};
my $start_time = time(); #starts the timer for the program

&usage;                                                                                    #Subroutine which checks that all the inputs are correct and fishes out the indata
&prep;                                                       #Subroutine which creates the necessary folder format (indata, unique dated directory)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#PART1: Prepares files for sigoli.

#this subroutine de-aligns the original alignment file and outputs $filename.notaln.fasta
#INPUT: ORIGINAL-ALIGNED-FASTA
#OUTPUT FILENAME.NOALN.FASTA in CURRENT DIRECTORY
my $st=time();
File_Convert::rewrite_fasta("$filename");

#This module generates the .nwk file from the .tre file. Output messages are surpressed with dev null.
#This module uses List::Compare
#INPUT: ORIGINAL-TREE ORIGINAL-OUTPUT
#OUTPUT TREEFILENAME.NWK, TREEFILENAME.NWK.NODELIST IN MAIN DIRECTORY (CHANGE THIS, MESSY)
#NODE LIST IN CURRENT DIRECTORY

File_Convert::add_treenodes("$in_tree", "$outgroup", $isolation,"$isofile");

#moves the made files out of indata and into the output directory
move ("$indir/$s_tree.nwk","$s_tree.nwk") or die;
move ("$indir/$s_tree.nwk.node.list","$s_tree.node.list") or die;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#PART2: Works with tree_and_nodes folder, which will contain the folders sigoli works on

#Creates tree_and_nodes folder and Nodes.list file
Tree::treeprep("$s_tree.nwk");
chdir ("tree_and_nodes");  

# This subroutine creates the folder system that sigoli uses. It needs the node list and .nwk tree file. 
#INPUT: IN_TREE.NWK NODES.LIST
#OUTPUT: Folder for each node in tree_and_nodes directory, with corresponding leaves in the folders.
Tree::createnodes("../$s_tree.nwk","../Nodes.list");

#Preform cleanup and backup at the end of step  
#backups tree_and_nodes 

move ("Node1.id","../Node1.id");                                                                     #moves the ID outside or Sigoli will complain
 
chdir ("..");                                                        #changes directories outside of tree in nodes as the folder needs to be searched

my $et=time();
$runtime_hash->{'Tree prep'} =join(" ",$et-$st,"seconds");
$st=$et;
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#PART2.5: Tree and Node tranversing and modification for sigoli

#tranverses the tree_and_nodes directory to find seqIDs and creates folders for each sequence found
#every seqid found in tree and nodes will be worked on
#each file thereby created will only contain one sequence
#each node folder will contain any node that it is a parent of, but it only goes one level. 
#for example,the tree Node1 - Node2 - Node3 will create 3 folders.  
Tree::tranverse($s_filename,$thread);

print ("\n"); 
$et=time();
$runtime_hash->{'Tree traversal'} =join(" ",$et-$st,"seconds");
$st=$et;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#PART3: Sigoli calls 

chdir ("tree_and_nodes");                                                                                #moves back into tree and nodes for sigoli.

#my $start_time = time(); #starts the timer for the program

#this sub uses the program sigoli to search through the tree and nodes 
#sigoli will output sigoli_$filename.strings.lenX.txt files depending on oligo max/min specified
#The thread count, oligo max/mins are used as input, as well as the original unaligned fasta file.
#This must be run inside the tree and nodes folder with the above input.
#OUTPUT: sigoli.filename.stringslenX.txt, where x depends on oligio min/max
Sigoli_Func::start_sigoli($thread, $oligo_length_min, $oligo_length_max, "$filename", "." );
print ("\n"); 
$et=time();
$runtime_hash->{'Sigoli'} =join(" ",$et-$st,"seconds");
$st=$et;


# creates a sigoli folder to store the useful outputs that sigoli produces.
chdir ("..");
mkdir ("sigoli.$s_filename");


#moves all sigoli files to new directory
my @oldfiles = glob "tree_and_nodes/sigoli_*";
my $newdir = "sigoli.$s_filename";
foreach my $of (@oldfiles){ #puts all newly created files into new directory
 move ($of,$newdir); 
}
#calculate total run time
#my $end_time = time ();
#my $run_time = $end_time - $start_time;
#
# The loop checks the specificity of every oligo length output by sigoli
#It finds the match of the designed oligo in the converted fasta file and gives the total number of matches.
#Also places the non-specific oligos and specific oligos into different files.
#In the same step, the specific files are convereted into OFP format
Sigoli_Func::specific_OFP_gen($oligo_length_min,$oligo_length_max,$s_filename,$thread);

print ("\n");
$et=time();
$runtime_hash->{'Specificity check'}=join(" ",$et-$st,"seconds");
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#PART4: Len25 and final outputs
#There will be tens of thousands of oligos designed by Sigoli. Len25 oligos are used as a basis to select oligos for the node.
chdir ("sigoli.$s_filename");

#my $start_time = time(); #starts the timer for the program

Output_Cleanup::combine_output; #various procedures that interpet sigoli's output
#calculate total run time
my $end_time = time ();
my $run_time = $end_time - $start_time;
 
#delete tree_and_nodes if set
if ($erase_tree == 1){
	chdir ("..");
	rmtree ("$output_dir/$folder/tree_and_nodes") or warn "Could not remove tree and nodes!\n";
}

#delete the sigoli folder if set
if ($erase_sigdata == 1){
	rmtree("$output_dir/$folder/sigoli.$s_filename") or warn;	
}
 
print "\nFinished running. Took a total of $run_time seconds.\n";


print ("\n\n\n===========END==========================================================================\n\n");

push (@runtime_array, {'Total'=> "$run_time seconds"});
push (@runtime_array, $runtime_hash);
LogFile::addToLog("Runtime","Running time",\@runtime_array);	
LogFile::writeLog("$output_dir/$folder/Output/$s_filename.yaml");

####END OF MAIN#####################################################################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
####################################################################################################################################################

sub config{
	#this sub reads from the configuration file and checks if everything is defined.
	open (CONF,"$config_path") or die;
	while (<CONF>){
		my $line = $_;
		chomp $line;
		my @split;
		if ($line =~ m/^THREAD_COUNT/){
			@split = split(/=/,$line);
			$thread = $split[1];		
		}elsif($line =~ m/^OLIGO_MIN/){
			@split = split(/=/,$line);
			$oligo_length_min = $split[1];
		}elsif($line =~ m/^OLIGO_MAX/){
			@split = split(/=/,$line);
			$oligo_length_max = $split[1];
		}elsif($line =~m/^OUTPUT_SPECIFIC_LEN/){
			@split = split(/=/,$line);
			$output_specific_len = $split[1];
		}elsif($line =~m/^ERASE_TREE/){
			@split = split(/=/,$line);
			$erase_tree = $split[1];
		}elsif($line =~m/^ERASE_SIGDATA/){
			@split = split(/=/,$line);
			$erase_sigdata = $split[1];
		}elsif($line =~m/^ERASE_BLASTRESULTS/){
			@split = split(/=/,$line);
			$erase_blastresults =$split[1];
		}elsif($line =~ m/^REFERENCE_FASTA/){
			@split = split(/=/,$line);
			$ref_fasta = $split[1];
			if(index($ref_fasta, "/") == -1){
				$ref_fasta="$FindBin::Bin/Reference/$ref_fasta";	
			}
		}elsif($line =~ m/^REFERENCE_TAX/){
			@split = split(/=/,$line);
			$ref_tax = $split[1];
			if(index($ref_tax, "/") == -1){
                                $ref_tax="$FindBin::Bin/Reference/$ref_tax";
                        }
		}elsif($line =~m/^INPUT_FOLDER/){
			@split = split(/=/,$line);
			$indir = $split[1];
		}elsif($line =~m/^OUTPUT_FOLDER/){
                        @split = split(/=/,$line);
                        $output_dir = $split[1];
                }
		LogFile::addToLog("Options","Configuration",{'Path' => $config_path,
							     'Threads' => $thread,
							     'Oligo Minimum Length' => $oligo_length_min,			
							     'Oligo Maximum Length' => $oligo_length_max,
							     'Oligo Specific Length' => ($output_specific_len == 0)?'Not used':$output_specific_len,
							     'Erased tree' => ($erase_tree==1)?'Yes':'No',
							     'Erased sigoli data' => ($erase_sigdata==1)?'Yes':'No',
							     'Erased blast results' => ($erase_blastresults==1)?'Yes':'No'});

	}
	
	#check if all the values are defined

	close CONF;
}

sub usage{
	#subroutine that provides usage information and checks input from the indata folder

#Checks if the fasta file exists. 

#AODP's options
my $help;
my $iso_opt;
my $unite_opt;
my $names_opt;
my $config;
GetOptions ("--help" => \$help,"-i" => \$iso_opt,"-u" => \$unite_opt,"-n" => \$names_opt,"-c=s" =>\$config)or die $!;
if(defined $config){
	#reads in config file
 	$config_path = $config;
	#check for config file existence 
 	unless(-e "$config_path") {
 		die "The specified config file: $config_path does not exist.\n";
	} 	
}
if(defined $help){ 
	#outputs messages about how to use the program 
	 #calls the man
	 system ("perldoc $FindBin::Bin/AODP.pl");
	 exit
 }
 &config;

#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);

print ("\n===========RUNNING================================================================\n");
print ("Using ",$thread, " threads and with Oligo minimum length of $oligo_length_min and maximum of $oligo_length_max :\n\n\n");

 #reads everything from the indata folder
 my @files = <$indir/*>;
 #checks for unite
 #this option can be used with the others
 if (defined $unite_opt){
	 $unite =1;
}
if (defined $iso_opt){
	 #isolate option is on. Program will only get the output from isolated patterns
	 $isolation = 1;
	 print ("Using -i option!\n");
} 
if(defined $names_opt){
	 $nameswitch = 1;
	 print ("Using names option!\n");
}
if (grep(/-/,@ARGV)){
	 #check for random options (obsolete)
	 print ("Invalid option\n");
	 exit;
}	
 
if ($isolation == 1  && scalar(@files) < 4){
	 #checks if isolation was set
	 print ("-i option was set. In addition to standard 3 input files, an additional 'iso' file is needed in the indata folder.\n");
	 exit;
}elsif($nameswitch == 1 && scalar(@files) < 4){
	 print ("-n option was set. In addition to the standard 3 input files, an additional 'names' file is required.");
	 exit;
}
else{
	 #finds all files in the indata folder. Calls autoinput for each file found
	 find ({wanted => sub {&autoinput;}, no_chdir => 1},$indir); 
	 LogFile::addToLog("IO","Input",$input_hash);
}

#check if the files actually exist
 if (! -e $filename){
	 die "Fasta file does not exist in specified directory.";
 }
#checks oligo length min/max, and that the max is greater or equal to the min.
 elsif ($oligo_length_min < 0 || $oligo_length_max < 0 || $oligo_length_max < $oligo_length_min){
	 die "Invalid Oligo lengths. Either they are negative or the max is less than min.";
 }
#checks that specify length is within the range of oligo_len_min and oligo_len_max
if(($output_specific_len < $oligo_length_min || $output_specific_len > $oligo_length_max) && $output_specific_len!=0){
	die "Specified oligo length must be between oligo_length_min and oligo_length_max\n";
}
#checks pre-defined outgroup pattern file exists
 if (! -e $outgroup){
	 die ("Outgroup file does not exist");
 }elsif (! -e $in_tree){ #checks that the tree exists
	 die ("Tree file does not exist");
 }

 if ($isolation == 1 && $isofile eq ""){
	 print ("ISO file was not found.\n");
	 exit;
 }

 if ($nameswitch == 1 && $namesfile eq ""){
	 print ("Names file was not found.\n");
	 exit;
 }
#checks that the thread is not negative
 elsif ($thread < 1){
	 die "Thread count can not be less than 1.";
 }
&tree_file_check($filename,$in_tree);
if($isolation){
        &iso_file_check($filename,$isofile);
        LogFile::addToLog("IO","Patterns",join("\n",&getpatterns($isofile)));
}

#get unite_patterns
if($unite==1){
	#check files
	if (! -e "$ref_fasta" || ! -e "$ref_tax"){
		die "Error in Reference files. Please check the filenames.\n";
	}

	print ("Using crosschecking option\n");
	&fasta_file_validation_check($filename,$ref_tax);
	&reference_database_check($ref_fasta,$ref_tax);
	print "\n";
	LogFile::addToLog("IO","Reference",{'Reference fasta file' => $ref_fasta,
					 'Taxonomy mapping file' => $ref_tax});									
}
#checks that sigoli is installed
 my @sigolitest = `sigoli --help`; #dry run of sigoli to see if program is installed
 if (!$sigolitest[1]){ #sigoli doesn't exist - exit
	 print ("Sigoli is not installed!");
	 #Call makefile from sigoli package, run sigoli.exe in folder
	 exit;
 }else {
	 #sigoli is installed!
	 print ("Sigoli was found to be installed.\n");
 }
 print ("Input paramaters are valid.\n");
 print ("- - - - - - - - - - \n");
}

#Get patterns from ISO file
sub getpatterns{
	my $filename=shift;
	open (ISO,$filename) or die $!;
        my @patterns =<ISO>;
	close ISO or die $!;
	#grep out the empty lines
        @patterns=grep (!/^\s$/,@patterns);
        chomp @patterns;
	return @patterns;
}


#Automatically gets needed files from indata folder
sub autoinput{
  my $found = $_;
  if ($found =~ m/\.fasta/){
    $filename = $found;
    print ("Using $found\n");
    $input_hash->{'Fasta file'} = $found;
  }elsif ($found =~ m/\.tre/){
    $in_tree = $found;  
    print ("Using $found\n");
    $input_hash->{'Tree file'} = $found;
	}elsif ($found =~ m/\.outgroup/){
		$outgroup = $found;
		print ("Using $found\n");
		$input_hash->{'Outgroup file'} = $found;
	}
	#options
	if ($isolation == 1 && $found =~ m/\.iso/){
		$isofile = $found;
	  print ("Isolation file is $found\n");
	  $input_hash->{'Isolation file'} = $found;	
	}
	if ($nameswitch == 1 && $found =~m/\.names/){
		$namesfile = $found;
		print ("Names file is $found\n");
		$input_hash->{'Names file'}= $found;
	}
}

##########################################################################################################
sub prep{ 
 #subroutine that sets up some variables and outdata folders
	
	#Checks if the outdata folder exists.
	if (! -e "$output_dir"){
		#Indata doesn't exist, create indata
		mkdir ("$output_dir");
	}
	LogFile::addToLog("IO","Output Folder",$output_dir);	
	chdir ("$output_dir"); #changes directories into the "outdata" directory. This has been made if not present.

	$full_filename = basename ($filename);                       #this removes any paths in front of the file.
	($f_extension = $full_filename) =~ s/^[^.]+\.//;              #sets the extension into the global variable
	($s_filename = $full_filename) =~ s/\.[^.]+$//;          #sets the small filename into the global variable
	$s_outgroup = basename ($outgroup);											#removes path of outgroup file
	$s_tree = basename ($in_tree); 														#removes path of tree


	my $DATE=POSIX::strftime('%Y_%B%d_%H_%M',localtime()); 
	my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M',localtime());                 #gets the date into the format YEAR/MONTH/HOUR/MINUTE
	#creates a directory in the format filename.index_and_sigoli.currentdate
	
	LogFile::addToLog('Date','Date and time',$HR_DATE);
		


	$folder = "$s_filename.$DATE"; 
	chomp $folder;        #chomp line is needed to remove uneeded lines, otherwise chdir will not be functional
	#Checks if there is already a folder with the name. 
	#In that case, the program has been run very recently, and a new folder does not need to be made.
	if (! -e $folder){
		mkdir ($folder) or warn ("Could not make $folder\n"); 
	}else{
		rmtree ($folder) or die ("Could not remove $folder from previous run\n");
		mkdir ($folder);
	}
	chdir ($folder);                      #moves into created folder

}
sub fasta_file_validation_check{
	my $fasta=shift;
	my $database=shift;
	open DATA, $database or die $!;
	my @database=<DATA>;
	close DATA or die $!;
	open FASTA, $fasta or die $!;
	print "Checking fasta file against ", basename($database,".fasta")," database...\n";
	foreach my $line (<FASTA>) {
        	chomp $line;
        	if($line!~m/>/){
               		next;
        	}
        	$line=~s/>//g;
        	my @seq_split=split(/_/,$line);
        	if(scalar @seq_split<3){
                	die "The input format isn't correct.\nThe incorrect identifier is $line\nThe correct format should be 'identifier1_identifier2_genus_species'\nEg.gi_14135661_Peronospora_farinosa\n";
        	}
        	#check if genus is in reference database
        	my $genus=@seq_split[2];
        	my @grep_check=grep(/$genus/,@database);
        	if(not @grep_check){
                	die "The species $line does not exist in the reference database.\n",
                	    "1.Is your format correct?\n\tThe format must be 'identifier1_identifier2__genus_species'\n\tEg.gi_14135661_Peronospora_farinosa\n",
                	    "2.Check your spelling\n",
                	    "3.Check if you used the right reference database for your dataset\n\tEg. don't use unite database for oomycetes\n",
                	    "4.Add it to your reference database\n";
        	}
	}
	print "Check passed!\n";
	close FASTA or die $!;
}
#checking tree and fasta file to see if they match
sub tree_file_check{
	my $fasta=shift;
	my $treefile=shift;
	my $tree_in=Bio::TreeIO->new(-file=>$treefile, -format=>'newick');
	my $tree=$tree_in->next_tree();
	my $in=Bio::SeqIO->new(-file=>"$fasta", -format=>"fasta");
	while (my $seq=$in->next_seq()) {
                        my $id=$seq->id();
                        my $node=$tree->find_node(-id=> $id);
			if(!defined $node){
				die "$id is not in the tree, please check your inputs.\n";
			}
        }

		

}
#checking isolation file to see if pattern exist in fasta file
sub iso_file_check{
	my $fasta=shift;
        my $in=Bio::SeqIO->new(-file=>"$fasta", -format=>"fasta");
	
	
	my $isofile=shift;
	my @patterns=getpatterns($isofile);
	my $good;
	foreach my $p (@patterns){
		#remove taxon identifiers
		$p=~s/^.*?__//g;
		#Goes through each sequence
		$good = 0;
		while (my $seq=$in->next_seq()) {
			my $id=$seq->id();
			if($id =~m/$p/){
				$good=1;
				last;
		        }
	        }
		if(! $good){
			die "isolation file pattern ",$p," is not in fasta file\n";
		}
	}

}
sub reference_database_check{
	my $fasta=shift;
	my $tax=shift;
	my $in=Bio::SeqIO->new(-file=>"$fasta", -format=>"fasta");

	print "Checking reference databases...\n";

	open TAX,$tax or die $!;
	my @taxonomy=<TAX>;
	my $tax_count=scalar @taxonomy;
	close TAX or die $!;
	my $count=0;
	while(my $seq=$in->next_seq()){
		$count++;
	}
	if($count != $tax_count){
		warn "Your reference fasta file has $count sequences, but your reference taxonomy file has $tax_count sequences. There may be a problem with your reference databases.\n";
	}	
}

__END__

=head1 NAME 

=over 12

=item B<A>utomated B<O>ligonucleotide B<D>esgin B<P>ipeline (AODP)

=back

=head1 SYNOPSIS

perl AODP.pl [-i] [-n] [-u] [-c config] [--help]

=head1 DESCRIPTION

The Automated Oligoneucleotide Design Pipeline extracts signature oligoneucliotides of specified lengths primarily using the progrom SigOli (Signature Oligo, Manuel Zahariev). Using these outputs from taxon groups of interest, they can be used to identify species with great accuracy.

To run, taget fasta file, tree, and outgroup list needs to be placed in the indata folder. 

B<IMPORTANT FASTA FILE MUST FOLLOW THIS FORMAT FOR OPTIMAL RESULTS: identifier1_identifier2_genusname_speciesname Ex. gi_2123211_Peronospora_farinosa.>

The script will automatically get the files from the folder. Make sure only the desired dataset is in the indata. Once the files have been placed, simply run the script using "perl AODP.pl" or "./AODP.pl". The default oligo lengths to search for are 25-28, using 24 threads. The user will need to set these values in the script to their preference.


=head1 OUTPUTS

A dated folder with the dataset name will be generated in the "outdata" folder. All output will be placed in this folder for the particular run. 

In this folder, a concrete not aligned version of the input fasta will be found. This contains the sequences with alignment and problematic sequences removed. These problematic sequences are placed in uncalculated.fasta in the same directory.

A folder with the sigoli folder can also be found. This folder contains the vanilla output from the SigOli program and the specific/OFP files for each oligo length. This folder is useful if output from a specific length is required, otherwise the next folder is useful.

The generated newick tree and it's related outputs(nodes list, outgroup nodes) are generated in the same directory.The IDs of all the sequences are placed in Node1.id, and a list of all the node counts are placed in Nodes.list. The node list is very useful for a concrete list of all the sequences that exist inside of a node.

The final folder is the "Modified Data" folder, and it contains the main output for the run. Output of length 25 will be captured here for the purpose of having a smaller subset of the data to put in the OFP pipeline. Additionally, the combined oligios of all lengths will be created here, containing everthing that was found.

The generated newick tree and it's related outputs(nodes list, outgroup nodes) are generated in the same directory.The IDs of all the sequences are placed in Node1.id, and a list of all the node counts are placed in Nodes.list. The node list is very useful for a concrete list of all the sequences that exist inside of a node. 

Conversely, there is a lineage.txt file that contains the full (Node) lineage for all the species in the tree.

If the -u option is specified, the superspecific versions of the output will also be created, there is also an additional node list (superspecific.descendents.tab) and a .eps file which colors in the tree with all the oligos designed. 


=head1 OPTIONS

Input options as parameters.

=over 12
	
=item B<--help> 

Ouputs program information and usage instructions. Further instructions are outlined in this document and the manual. 
	
=item B<-i>	

"Isolate" option. Requires an additional file in the indata folder with ".iso" as the extension. This file should contain a list of species you wish to isolate eg. peronospora_farinosa. The sequence names outlined in the file will the isolated in the final output, as well as the nodes that contain only those sequences. 

=item B<-u>

"UNITE" cross-referencing option. The generated oligos will further be checked for specificity after on a species level. 

B<FASTA FILE NEEDS TO FOLLOW THIS FORMAT: identifier1_identifier2_genusname_speciesname Ex. gi_2123211_Peronospora_farinosa> or the program will stop and give an error message.  

=item B<-n>

"Name" file switch. Allows the AODP to utilize name files. Simply place the names file in the indata folder along with the standard input. Make sure that the target fasta and tree are already unique. The names file will be integrated into the node list.

=item B<-c>

Specify the path of the config file used for additional options such as number of threads, reference database files, etc.

=back

=head1 BUGS

Sigoli is, as of right now, not optimized for sequences with too many ambiguous bases. The program will remove sequences with over 5 ambiguous bases, and note these sequences in uncalculated.txt

=cut


