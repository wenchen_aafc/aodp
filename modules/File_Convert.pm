#! /bin/usr/env perl
package File_Convert;
use File::Basename;
use Bio::TreeIO;
use Bio::SeqIO;
use File::Basename;
use File::Copy;
use FindBin;
# Part of AODP pipeline.
# This module contains functions to convert an aligned fasta file into unaligned and convert a tree file into an nwk tree file.
# Additionally outputs the node list.

#MIT License
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Sigoli: SigOli is now distributed under the GNU General Public License (GPL).

#DOI: 10.5281/zenodo.10762

# authors: Kaisheng (Kevin) Chen, Chuyang (Charles) Qi, Wen Chen* (wen.chen@agr.gc.ca)

use warnings;
use strict;

#BAD IN FILENAME, they need to be removed. "-" is kept.
my @special_char=("/", "*", "__", "\\","(",")",":",";",",",".");

#subroutine takes in an ALIGNED fasta file and outputs the aligned version
#output file will be in the format name.notaln.fasta
sub rewrite_fasta {

	#checks for fasta file and cuts the leading paths
	my $fasta=shift or die ("Please provide a fasta file with '.fasta' as the extension");
	my $basename=basename ($fasta, ".fasta");
	my @problembases =qw(K M R Y S W B V H D N);
	my $count = 0;
	my $amb_limit = 5;
	my $ignored = 0;

	#calls bioperl
	my $in=Bio::SeqIO->new(-file=>"$fasta", -format=>"fasta");
	open (OUT, ">$basename.notaln.fasta") or die; #Creates $filename.notaln.fasta in directory
	open (UNC,">excluded.fasta") or die; #file to keep track of ignored sequences

	my %seen;
	#Goes through each sequence
	while (my $seq=$in->next_seq()) {
		my $id=$seq->id; #Gets the ID through bioperl (Axxxxx Pectobacterium)
		#check the name file. If it's a reference set, continue. If not, discard

		#replace special characters with underscore
		foreach (@special_char){
			if ($id=~m/\Q$_\E/){
				$id=~s/\Q$_\E/_/g;
				warn "Your fasta file has a $_ in one or more of its ids. They have been replaced by _. It is considered bad practice to have $_ in your sequence identifiers.\n";
			}
		}
		if ($main::nameswitch == 1){
			open (NAMES,"$main::namesfile")or die;
			my @namegrep = grep(/$id/,<NAMES>);
			close NAMES;
			if (scalar @namegrep == 1){
				#splits the name
				my  @splitname = split (/\t/,$namegrep[0]);
				#checks if the id falls into second. If so, don't use it. 
				if (index($splitname[0],$id) == -1){
					#the id exists in the second column. 
					next;
				}
			}elsif(scalar @namegrep == 0){
				warn "ID doesn't appear in names file ($id).\n";
			}else{
				warn "ID appears more than once in names file ($id)\n";
			}

		}
		if ($seen{$id}++) {
			next;
		}else {
			my $string=$seq->seq;
			$string=~ s/-//g; #Splices the - from the string if found
			$count++ for grep/[@problembases]/i, split //, uc($string);
			if (index(uc($string),"XER") == -1 && $count < $amb_limit){ 
				#don't allow more than n occurances of ambiguity characters
				print OUT ">", $id, "\t\n", "\U$string\E", "\n"; #prints valid output without alignement
			}else{
				#contains too many ambiguity
				$ignored++;
				print (UNC "$id\t Ambiguity: $count\n","\U$string\E\n");
			}
			
			$count =0;
		}

	}
	#message to console when step is done
	print ("Created $basename.notaln.fasta, but ignored $ignored sequences.\n");
	close OUT;	
	close UNC;
}

###############################################################################################

# subroutine takes in the .tre file and outgroup file to add the nodes to the tree, creating nwk files. Additionally, it will generate the node numbers that are for the outgroup
# output format is filename.tre.nwk.node.list and filename.tre.nwk and outgroup.nodes
sub add_treenodes{

	#input parameters
	my $file=shift or  die "Could not locate tree file";
	my $outgroup=shift or die "Could not locate outgroup";
	my $isofile;
	#iso option?
	my $iso = shift;
	if ($iso == 1){
		#gets the isolation file
		$isofile = shift;
	}
	my $filename= basename("$outgroup");
	#calls bioperl to newick format
	my $tree_in=Bio::TreeIO->new(-file=>$file, -format=>'newick');
	my $tree_out=Bio::TreeIO->new(-file=>">$file.nwk", -format=>'newick');
	my $count=1; #count variable used to tranverse tree

	while (my $tree=$tree_in->next_tree) {
		foreach my $node ($tree->get_nodes) {
			#puts the node in the tree with the specified count
			if ($node->is_Leaf){
				#replace forward slash with _
				my $node_id=$node->id;
				foreach(@special_char){
					if ($node_id=~m/\Q$_\E/){
						$node_id=~s/\Q$_\E/_/g;
						$node->id("$node_id");
					}
				}
			}
			else{
				$node->id("Node$count")
			}
			#increment the count for the next clade
			$count++;
		}
		$tree_out->write_tree($tree);
	}

	#tells biotree to go into newick mode
	my $treeio=Bio::TreeIO ->new(-format => "newick", -file => "$file.nwk");
	my $treenwk = $treeio->next_tree; 
	#tree functions to get next tree/node
	my $rootnode = $treenwk->get_root_node;
	my $rootnodeid=$rootnode->id;
	# process just the all generations
        

#===========================================================NODE LIST GENERATION (REMOVED IN V1.1)================================================================
	open (OUT, ">$file.nwk.node.list"); #node list
	my $nodes;
	$nodes=();
	my $node;
	foreach my $root_childs ($rootnode->get_all_Descendents()){
		my $rootchildsid = $root_childs->id;
		#	print $node, "\n";
		if ($rootchildsid=~ m/Node/) {
			$node=$rootchildsid;
			$nodes->{$node}->{'NODE'}=$node;
			foreach my $child_node ($root_childs->get_all_Descendents() ) {
				my $node_allid=$child_node->id;
				$node_allid=~ s/'//g;
				unless ($node_allid=~ m/Node/) {
					$node_allid=~ s/'//g;
					push @{$nodes->{$node}->{'LEAVES'}}, $node_allid;
				}
			}
		} else {
			$rootchildsid=~ s/'//g;
			print OUT $rootchildsid, "\t", $rootchildsid,"\n";

		}
	}		

	foreach $node (sort keys %$nodes) {
		print OUT $nodes->{$node}->{'NODE'}."\t";
		print OUT join (",", @{$nodes->{$node}->{'LEAVES'}}), "\n";
	}
	close OUT;

	#do more if names option is on
	if ($main::nameswitch == 1){
		&names_fusion($main::namesfile,"$file.nwk.node.list",$file);
	}	
#=======================================================================================================================================================================
            
	open (FH, "<$outgroup")or die;
	my @array;
	while (<FH>){
		chomp $_;
		push @array, $_;
	}
	close FH; 

	@array=sort @array;
      
    
	#This part also checks the isolation file!
	my $iso_expr;
	my @isoarray;
	if ($iso == 1){
		open (ISO,$isofile)or die "Can't open isolation file\n";
		@isoarray=<ISO>;
		@isoarray=grep (!/^\s$/,@isoarray);
		foreach(@isoarray){
			chomp $_;
			#remove taxon identifiers
			$_=~s/^.*?__//g;
		}
		$iso_expr = join ("|",@isoarray);
		#print $iso_expr, "\n";
		close ISO;
	}

	#array is simply the outgroup
        #this join is a clever way of greping one array against the other
        my $expr = join ("|",@array);

	open(OUT1, ">$filename.nodes") or die;
	foreach $node (sort keys %$nodes) {
		my @allleaves=sort @{$nodes->{$node}->{'LEAVES'}};
		#if -i is set, we only want nodes that contain the stuff in iso_expr!
		if ($iso == 1){
			foreach my $leaf(@allleaves){
				#print "$leaf\t";
				unless($leaf =~m/$iso_expr/ or $leaf =~m/Node/){
					print OUT1 $node, "\n";
					#print $node, "\n";
					last;
				}	
			}
			#print "\n";
		}
                elsif (! -z $outgroup && scalar(grep/$expr/,@allleaves) > 0){
			#expr is a single element in the outgroup. It checks if it exists in @allleaves, which are all the leaves in the node
			#its outgroup!
			print OUT1 $node, "\n";
		}
		#checks if there are outgroups in these leaves. If there are, output that node to a list
		#my $lc = List::Compare->new('--unsorted', \@allleaves, \@array)
	}
	print OUT1 "other","\n";
	close OUT1;

	print "Created nwk files...\n";
	#write lineage file
	&tree_lineage($file);
  
}

sub tree_lineage{
	my $path=shift;
	#open a new file to write the lineage
	open (LINEAGE_FILE,">$main::s_tree.lineage.txt") or die $!;
	my $treeio= Bio::TreeIO ->new (-format => 'newick', -file => "$path.nwk") or die $!;
	my $tree=$treeio ->next_tree; 
	#get a list of all nodes and leaves in the tree
	my @nodes = $tree->get_nodes();
	foreach my $node (@nodes) {
		if ($node->is_Leaf()) {
			my $node_id = $node->id();
			#get the lineage of the node or leaf
			my @lineage = $tree->get_lineage_nodes($node);
			foreach (@lineage){
				#print the lineage ids in colon format
				my $s_lineage_id=$_->id();
				print LINEAGE_FILE "$s_lineage_id:";
			}
			#append the current id to the lineage 
			print LINEAGE_FILE "$node_id\n";
		}
	}
	#do more if names option is on
	if ($main::nameswitch == 1){
		&lineage_fusion($main::namesfile,"$main::s_tree.lineage.txt");
	}
	close LINEAGE_FILE or die "Cannot close file";
}




sub lineage_fusion{
	#incorporates the names file into the lineage file. -n option only
	my $namesfile = shift;
	my $orig_lineage = shift;
		
	#read names file into hash
	my %names_hash;
	open (NAMES,$namesfile) or die $!;
	while(my $cur_line= <NAMES>){
		chomp($cur_line);
		my @line = split (/\t/,$cur_line);
		my $original=$line[0];
		my $identical=$line[1];
		$names_hash{$original}=$identical;
	}
	close NAMES or die $!;
	#open new file for writing
	open(NAMESLINEAGE,">$main::s_tree.names.lineage") or die $!;
	#open original lineage file
	open(LINEAGE,$orig_lineage) or die $!;
	#append all identical nodes to lineage
	foreach my $line (<LINEAGE>){
		chomp($line);
		my $key=substr($line, rindex($line,":")+1);
		my $value="";
		$value=$names_hash{$key};
		#if the value is not in the hash, then it is not a representative sequence
		if (defined $value){
			#remove key and append value
			$line =~s/$key/$value/g;
			print NAMESLINEAGE "$line\n";		
		}
	}
	#replace old file with new file
	move("$main::s_tree.names.lineage",$orig_lineage);
	close NAMESLINEAGE or die $!;
	close LINEAGE or die $!;
}
sub names_fusion{
	#incorporates the names file into the node list. -n option only
	my $namesfile = shift;
	my $orig_node = shift;
	my $treefile = shift; #just need the name

	#open the new file
	open (NAMESNODE,">$treefile.names.nodelist") or die $!;

	#read the names file into memory, to be pasted later
	open (NAMES,"$namesfile") or die $!;
	my @allnames = <NAMES>;
	close NAMES;
	
	#read the nodes list file.
	open (NODELIST,$orig_node) or die;

	#print the names file in the output
	print NAMESNODE @allnames;

	while (<NODELIST>){
		#check if it is a node. If not, get rid of it.
		if ($_ =~ m/Node/){
			#get the reference sequences. Grep the names file to get the identicals
			my @identical;
			
			#split the line
			my @line = split (/\t/,$_);
			my @refseqs = split(/,/,$line[1]);
			foreach (@refseqs){
				#find the identicals, push onto the array
				my $curseq = $_;
				chomp $curseq;
				open (NAMES,"$namesfile") or die;
				my @namesline = grep(/$curseq/,<NAMES>);
				close NAMES;
				#get the line with the seq we want
				my @nameslines = split (/\t/,$namesline[0]);
				my $idenlist;
				if (scalar @nameslines > 0){
					#see if it was found
					#the second col contains the non-ref sequences
					#get the non-ref and push it onto the array
					#pop the first element though
					$idenlist = $nameslines[1];		
					chomp $idenlist;
					$idenlist =~ s/\t//;
					$idenlist =~ s/\s//;
				}
				push (@identical,"$idenlist,");
			}
			chomp $_;
			chomp @identical;
			print NAMESNODE "$_\t",@identical,"\n";
			
		}
	}
	#replace old file with new file
	move("$treefile.names.nodelist","$orig_node");        
	close NAMESNODE;
	close NODELIST;
}
1;

__END__
=head1 NAME

File Conversion Module for AODP

=head1 SYNOPSIS

	use File_Compare.pm
	
	rewrite_fasta("fasta");
	add_treenodes("tree","bool","outgroup");

=head1 DESCRIPTION

This module is used by the AODP to convert it's standard input data into the desired input. Namely,the rewrite_fasta function will eliminate any alignments that the original fasta had, and remove sequences with too many ambiguity letters. The add_treenodes function will add node counts to the original input tree if they did not exist.


