#!/usr/bin/env perl
package LogFile;

use strict; use warnings;
use YAML;
use YAML::Dumper;


#Part of AODP pipeline.
# This module contains functions to write a record events into a logfile.

##MIT License
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Sigoli: SigOli is now distributed under the GNU General Public License (GPL).

#DOI: 10.5281/zenodo.10762

# authors: Eric Hardy, Chuyang (Charles) Qi, Wen Chen* (wen.chen@agr.gc.ca)

my $_logfile;
my @logSections = ('Date','Options','IO','Runtime');

sub addToLog {
  my $section  = shift();
  my $header   = shift();
  my $log_data = shift();

  my $valid_section = 0;
  foreach (@logSections){
    $valid_section = 1 if($section eq $_);
  }
  die("[ERROR:] Bad logfile section\n") unless $valid_section;

  $_logfile->{$section}->{$header} = $log_data;
}

sub writeLog { #assumes all sections are filled in
  my $log_path = shift();
  my $yaml = YAML::Dumper->new();

  open(my $lf, '>', $log_path);
  foreach my $section (@logSections) {
    #$yaml->[$i] = $_logfile->{$logSections[$i]};
    print $lf ($yaml->dump($_logfile->{$section}));
  }
  close($lf);
}

1;
