#! /bin/usr/perl

package Output_Cleanup;
use List::MoreUtils qw/uniq/;
use File::Find;
use File::Path;
use File::Basename;
use FindBin;
use File::Copy;
use threads::shared;
use Thread::Queue;
use Bio::TreeIO;
use Bio::Tree::Draw::Cladogram;
use Fcntl qw(:flock);
use POSIX qw(:sys_wait_h);
$| = 1;

use warnings;
use strict;

# Part of AODP pipeline.
# This module eliminates oligos that are not specific for the sequence(s) or nodes they are designed for.

#MIT License
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Sigoli: SigOli is now distributed under the GNU General Public License (GPL).

#DOI: 10.5281/zenodo.10762

# authors: Kaisheng (Kevin) Chen, Chuyang (Charles) Qi, Wen Chen* (wen.chen@agr.gc.ca)
my %node_hash;

sub combine_output {
	#This subroutine is run near the end of the program to produce 5 files
	#It will create the files for the specified length, and combine a fasta file for oligos of all lengths.
	#The outgroup will then be excluded.
	#This subroutine  will work in the "Output" folder
	#combines and sorts the file, and deletes the outgroup sequences from specified length only

	#if ($main::oligo_length_min > 25 || $main::oligo_length_max < 25){
	#	print "WARNING: Oligo length 25 is not in specified oligo range.\n";
	#}
	print "Preparing final outputs";
	
	#Gets the name of sequences with specified length, cuts - and sorts into a list.
	#The outgroups will be taken out depending on this file
	if ($main::output_specific_len != 0){
		open (TOS,"sigoli_$main::s_filename.strings.len$main::output_specific_len.txt.oligo.specific.withLonghomo.tab") or die "Oligo length is not in the specfied oligo range\n";
		open (NLN,">node.len$main::output_specific_len")or die;
		my @nodelenspecific; #array used for sort and uniq 
		while (<TOS>){ #tranverses the file line by line
			my @splitter = split (/-/); #splits up by - to only get the name
			push (@nodelenspecific,"$splitter[0]\n"); #push the name onto the array
		}

		print ".";
		#sorts and uniq the array
		@nodelenspecific = uniq @nodelenspecific;
		@nodelenspecific = sort @nodelenspecific;
		#print out to new file
		print NLN @nodelenspecific;
		close NLN;
		close TOS;

		#goes through each file and outputs into extra nodes
		open (EXTRA,">extra.nodes");
		open (GRE,"node.len$main::output_specific_len") or die;
		my @togrep = <GRE>;

		#gets any extra nodes in all lens and gathers them together
		#also greps out any lines that belong to the outgroup 
		for (my $j = $main::oligo_length_min;$j<=$main::oligo_length_max;$j++){
			open (OFP,"sigoli_$main::s_filename.strings.len$j.txt.oligo.specific.withLonghomo.tab") or die; 
			my @ofpfile = <OFP>;
			foreach my $b (@togrep){ #Checks every node in file
				chomp $b;
				@ofpfile = grep !/$b/, @ofpfile; # takes the lines that contain NO outgroups
			}
			print EXTRA @ofpfile;
		}
		close EXTRA;
		close GRE;
		close OFP;
	}
	chdir ("..");

	#removes some uneeded files
	unlink ("Node1.id") or warn;
	unlink ("Nodes.list") or warn;

	#removes nodal files

	#outdata is created to place the final files
	mkdir ("Output");
	chdir ("Output");

	#Moves some files into this folder
	move ("../$main::s_tree.node.list","$main::s_tree.node.list") or warn;
	move ("../$main::s_tree.nwk","$main::s_tree.nwk") or warn;
	move ("../$main::s_tree.lineage.txt","$main::s_tree.lineage.txt") or warn "Cannot move $main::s_tree.lineage.txt";
	#Moves the notaln and excluded too
	move ("../$main::s_filename.notaln.fasta","$main::s_filename.notaln.fasta") or warn;
	move ("../excluded.fasta","excluded.fasta") or warn;

	#Creates outgroup patterns, which is a list of sequence names to be excluded from the lenall files.
	open (MYOUT,"$main::outgroup") or die "Can't find outgroup";
	open (NODES,"../$main::s_outgroup.nodes") or die "Outgroup nodes can't find";
	open (OALL,">$main::s_outgroup.all");

	#if -i was set, opens the iso files
	my @isoarray;
	my $iso_expr;
	if ($main::isolation == 1){
		open (MYISO,"$main::isofile");
		@isoarray = <MYISO>;
		@isoarray=grep (!/^\s$/,@isoarray);
		foreach(@isoarray){
                	 chomp $_;
                         #remove taxon identifiers
                         $_=~s/^.*?__//g;
                 }     
		 #also pushes "node" onto the array
		 push(@isoarray,"Node"); 
		 $iso_expr = join ("|",@isoarray);
		 #print $iso_expr, "\n";
	}

	#Goes through outgroup and adds - to the end line
	while (<MYOUT>){
		chomp $_;
		print OALL "$_\n";
	}

	print OALL <NODES>; #Concatenates onto the new file
	#Close filehandles
	close MYOUT;
	close NODES;
	close OALL;

	#opens the OFP files of specified length for grepping
	if ($main::output_specific_len != 0){
		open (FH,"../sigoli.$main::s_filename/sigoli_$main::s_filename.strings.len$main::output_specific_len.txt.oligo.specific.withLonghomo.tab") or warn;

		open (FH2, "../sigoli.$main::s_filename/extra.nodes") or warn;
	}
	open (GR,"$main::s_outgroup.all"); #outgroup that needs to be grepped out 

	my @grepgroup = <GR>; #reads the grep file into memeory
	chomp @grepgroup;
	#makes the grep expr
	my $grep_expr = join ("|",@grepgroup);
	if ($main::output_specific_len != 0){
		open (NF,">sigoli.$main::s_filename.len${main::output_specific_len}andother.tab.exOGtest"); #new file sorted and greped
		print ".";


		#concatenate the 2 files
		print NF <FH>;
		print NF <FH2>;
		close NF;


		open (RF,"sigoli.$main::s_filename.len${main::output_specific_len}andother.tab.exOGtest"); #reads the concatenated file
		#greps out the outgroup from specified length file aswell
		my @arr = sort <RF>;
		close RF;
		@arr = grep !/$grep_expr/,@arr;
		#also remove sequences that are not ones defined in isolate
		if ($main::isolation == 1){
			@arr = grep /$iso_expr/,@arr;
		}
		print ".";

		open (NF2,">$main::s_filename.len$main::output_specific_len.tab");
		#merge into tempsort prints the grepped file 
		print NF2 @arr;
		unlink ("sigoli.$main::s_filename.len${main::output_specific_len}andother.tab.exOGtest") or warn ("Could not remove OFPtest!\n");
		#close all filehandles
		close FH;
		close FH2;
		close GR;
		close NF2; 
		#///
		#combines all the len files and sorts.
	}
	my @all;
	for (my $j = $main::oligo_length_min;$j <= $main::oligo_length_max;$j++){
		open (AF,"../sigoli.$main::s_filename/sigoli_$main::s_filename.strings.len$j.txt.oligo.specific.withLonghomo.tab") or warn;
		print ".";
		push (@all, <AF>); #combines all the files and pushes onto the array
	}
	#greps the array
	@all = grep !/$grep_expr/,@all;
	#also remove sequences that are not ones defined in isolate
	if ($main::isolation == 1){
		@all = grep /$iso_expr/,@all;
	}
	#sorts and prints to the file
	open (ALF,">$main::s_filename.lenall.tab");
	@all = sort @all;
	print ALF @all;
	close AF;
	close ALF;
	           	
	#use this lenall in xref to make it more specific.
	#delete this file afterwards, convert new fastas.
	&fasta_to_OFP("$main::s_filename.lenall.tab");
	if ($main::output_specific_len != 0){
		&fasta_to_OFP("$main::s_filename.len$main::output_specific_len.tab");
	}

	&fasta_to_gff3("$main::s_filename.lenall.fasta","$main::output_dir/$main::folder/Output/$main::s_filename.notaln.fasta");
	if ($main::output_specific_len != 0){
		&fasta_to_gff3("$main::s_filename.len$main::output_specific_len.fasta","$main::output_dir/$main::folder/Output/$main::s_filename.notaln.fasta");
	}
	
	if ($main::unite == 1){
		my $st = time();
		print ("Cross validating with the database:\n");
		#call xrefs
		Xref($main::unite_pattern,"$main::s_filename.lenall.fasta","$main::s_filename.lenall.tab");
		if ($main::output_specific_len != 0){
			Xref($main::unite_pattern,"$main::s_filename.len$main::output_specific_len.fasta", "$main::s_filename.len$main::output_specific_len.tab");
		}
		my $et = time();
		$main::runtime_hash->{'Cross validation'} = join (" ", $et-$st, "seconds");
	}
	#this function outputs tab max4homo files based on the specified_length_andother file
	#Sigoli_Func::OFP_multilen("$main::s_filename.specfied_length_andother");
	unlink ("../$main::s_outgroup.nodes")or warn;
	#delete full node list since its redundant
	#unlink("$main::s_tree.node.list") or warn $!;

}

my @taxbase;
my @ref_blast;
#% counts
my $total;
my $current : shared;
$current = 0;

sub Xref{
	my $pattern = shift;
	#print "$pattern\n";
	my $oligo_file = shift;
	my $oligo_tab_file=shift;
	my $s_oligo = basename ($oligo_file,".fasta");
	my $pcount=0;
	my $len = shift;
	$pattern=get_patterns_from_oligos($s_oligo);
	my $reffasta=$main::ref_fasta;
	my $reftax=$main::ref_tax;

	my $ref_fasta = "$reffasta";
	my $ref_tax = "$reftax";
        
        my $s_ref_fasta=basename($ref_fasta,".fasta");
	
        open (TAX,"$ref_tax")or die;
	@taxbase = <TAX>;
	close TAX;

	mkdir ("tmp");
        File::Path::make_path("tmp/good", "tmp/bad");
	
	#making the blast database if it doesn't already exist
	unless(-e "$ref_fasta.nhr"){	
        	if( system("makeblastdb -in $ref_fasta -dbtype 'nucl'")!=0){
                	die "Error executing system command\n";
       		}
	}
        
        #blasting the oligo against the reference database 
        #output is 25 column tabular output
        my $blast_str="Blasting the oligos against the $s_ref_fasta database";
	print "$blast_str\r";
	
	#use fork to show blast progress--------------------------
	defined(my $pid = fork) or die "Couldn't fork: $!";

	if (!$pid) { # Child
  		exec("blastn -query $oligo_file -db $ref_fasta -evalue 0.001 -task blastn -out $s_oligo.blastn.tab -word_size $main::oligo_length_min -dust no -outfmt '6 qseqid sseq sseqid qlen qstart qend mismatch gaps' -num_threads $main::thread") 
    		or die "Couldn't exec: $!";
	} else { # Parent
		my $counter=0;
		my $str;
  		while (! waitpid($pid, WNOHANG)) {
    			if($counter<3){
				$blast_str =~ s/^\s+|\s+$//g ;
				$blast_str.=".";
			}
			else{
				$counter=-1;
			        $blast_str="Blasting the oligos against the $s_ref_fasta database   ";
			}
			print "$blast_str\r";
			$counter++;
    			sleep 2;
  		}
  		print "\n";
	}

	#------------------------------------------------------------
	#gets the line count from blast output
	open (COUNTER,"$s_oligo.blastn.tab");
        while (<COUNTER>){}
        $total += $.;                        
	close COUNTER;
	                                        
	#read blast output into an array
	open (BLAST_OUTPUT,"$s_oligo.blastn.tab")or die;
        my @blast_output = <BLAST_OUTPUT>;
        close BLAST_OUTPUT;

	
	print "Processing blast output:\n";
		
	
	#get unique seqids from tabular blast and write to file
	my $cur_id;
	my @blast_split;
	my %seen;
	open(my $unique_seqs,">","tmp/unique.tmp") or die;
	foreach (@blast_output){
		@blast_split=split(/\t/,$_);
		$cur_id= $blast_split[0];
		unless($seen{$cur_id}){
			$seen {$cur_id} =1;
			print $unique_seqs "$cur_id\n";
		}
	}
	
	#write all oligos not in blast results to file
	open (NOT_IN_BLAST,">tmp/not_in_blast.tmp")or die;
	print NOT_IN_BLAST `grep -vFif "tmp/unique.tmp" $oligo_tab_file`;	



	#get a list of all hits with the same sequence id and write each list to @all_seqs
	my @seq;
	my @all_seqs;
        @blast_split =split(/\t/,$blast_output[0]);
        my $prev_id=$blast_split[0];
        $cur_id=undef;
        # separate blast output by oligo id
	foreach(@blast_output){
		@blast_split =split(/\t/,$_);
		$cur_id=$blast_split[0];
		unless($cur_id eq $prev_id){
			push(@all_seqs,[@seq]);
			$prev_id=$cur_id;
			@seq=();	
		}			
		push(@seq, $_);
	}
        # push last sequence onto all_seqs
        push(@all_seqs, [@seq]);

	#read into hash
	my %oligo_hash;
	my $seqio = Bio::SeqIO->new(-file => "$oligo_file", -format => "fasta");
	while(my$seqobj = $seqio->next_seq) {
    		my $id  = $seqobj->display_id;    # there's your key
    		my $seq = $seqobj->seq;           # and there's your value
    		$oligo_hash{$id} = $seq;
	}
	my $hash_ref =\%oligo_hash;

	#read nodelist into hash
        open (NODELIST,"$main::s_tree.node.list") or die $!;
        while(my $cur_line= <NODELIST>){
        	chomp($cur_line);
                my @line = split (/\t/,$cur_line);
                my $key=$line[0];
                my $values=$line[1];
                $node_hash{$key}=$values;
        }
        close NODELIST or die $!;
        


	#do the work on each @seq using multiple threads
	my $queue = Thread::Queue->new(@all_seqs);
	my @threads;
	for my $i(1 .. $main::thread){ 
			push @threads, threads->new( sub {
				# Pull work from the queue, don't wait if its empty
				while( my $hits_ref = $queue->dequeue_nb ) {
					my @hits=@$hits_ref;
					&blastcheck($pattern,$hash_ref,@hits);
				}
			});
	} 

	$_ -> join for @threads;
	print "100.0%\r";
	
	close $unique_seqs;
	if($main::erase_blastresults==1){
		unlink("$s_oligo.blastn.tab");
	}		
	&combine_all("$s_oligo.superspecific.tab", "notspecific.seq");
	#output nodelist if names file is given
	&cross_validate_nodelist($s_oligo);
        &color_oligo("$s_oligo.superspecific.descendents.tab");	
	
	#this function outputs tab max4homo files based on the superspecific tab file
	Sigoli_Func::OFP_multilen("$s_oligo.superspecific.tab");
	

	close GOOD;
	close BAD;
	close NOT_IN_BLAST;
	
	rmtree("tmp") or warn;

}



sub blastcheck{
	#`````````````````````````````````````
	my $pattern=shift;
	my $hash_ref=shift;
	my %oligo_hash=%$hash_ref;
	my @tab_blast= @_;

	#getting sequence and id of currentline
	my @tab_split =split (/\t/,$tab_blast[0]);
        my $cur_id= $tab_split[0];
	my $cur_seq;
	#update progress
	$current+=scalar @tab_blast;
	printf("%.1f%%\r",($current/$total)*100);

	open (GOOD,">tmp/good/$cur_id.tmp")or die;
        open (BAD, ">tmp/bad/$cur_id.tmp") or die;

	my @taxonomy;
	foreach(@tab_blast){
		#parsing blast result data
		my @fields = split (/\t/,$_);
		my $gi_num= $fields[2];
		my $query_len = $fields[3];
		my $seq_len=$fields[5]-$fields[4]+1;
		my $mismatches = $fields[6];
        	my $gaps =$fields[7];
		$cur_seq=$fields[1];
		
		#checking for perfect match (no gaps and mismatches for the ENTIRE sequence)		
		if (($seq_len==$query_len) && $mismatches==0 && $gaps==0){
			#print "Perfect match\n";	
			#there is a identifical match in blast, checking the taxonomy database...	
			chomp $gi_num;
			if($gi_num =~m/\|/){
				$gi_num=~s/^gi\|//g;
				$gi_num=~s/\|//g;
			}
			
			#got a name. Check the line in the taxon
			my @line = (grep/$gi_num/,@taxbase);
			if(! @line){
				warn "$gi_num for $cur_id cannot be found it the taxonomy database.\n";
				next;
			}
			#line[0] should always exist
			#getting only the taxonomy
			my @tab_split=split("\t",$line[0]);
			my $s_taxonomy = $tab_split[1];
			chomp $s_taxonomy;
			#print $s_taxonomy,"\n";
			#get rid of strain
			my $index=index($s_taxonomy, $pattern);
			#get rid of useless classification, ie. if looking for genius, get rid of species classification in the taxonomy
			if($index >= 0){
				$s_taxonomy=substr($s_taxonomy,0,$index+length($pattern)-1);
				#print "$s_taxonomy\n";
			}
			
			#print "$s_taxonomy\n";
			push(@taxonomy,$s_taxonomy);
			if ($tab_split[1] !~ m/$pattern/){
				#One of the taxonomies don't match the specified patterns. It's no good.
				chomp $tab_split[1];
				#this prints the FIRST mismatch, not all!
				print BAD $cur_id,"\tReason:$tab_split[1]\n$cur_seq\n";
				return;
			}
        	}
	}
	
	@taxonomy=sort(@taxonomy);
	@taxonomy=uniq(@taxonomy);	
	
	#get full sequence
        $cur_seq=$oligo_hash{$cur_id};
        chomp $cur_seq;
	#oligo is designed for only ONE of the taxonomy patterns, so it is good
	if(scalar @taxonomy ==1){
		 print GOOD $cur_id, "\t$cur_seq\n";
	}

	#oligo is designed for more than one taxonomy in the .iso file
	elsif(scalar @taxonomy>1){ 
		#if it is a node, then there is some work to do
		if($cur_id=~m/Node/){
			#grep out the oligo part
			my @split=split(/-len[0-9]/,$cur_id);
			my $specific_id=$split[0];
			#print "$specific_id\n";

			#get all descendent in the tree from node list file
			my $children=$node_hash{$specific_id};
			my @childs=split(",",$children);
			@childs=grep{$_!~m/Node/}@childs;
			$children=join("|",@childs);
			#print "$children\n";
			#print "$pattern\n";
			my @patterns=split(/\|/,$pattern);

			#get all patterns that were found
			my @found_pattern;
			#print join("\n",@patterns);
			foreach my $line(@taxonomy){
				
				chomp $line;
				#print "$line\n";
				foreach my $p(@patterns){
					if($line =~m/$p/){
						push(@found_pattern,$p);
					}
				}
			}
			#print join("\n",@found_pattern), "\n";

			#check that all patterns found are actually children of the node, if every pattern found is a children, then the oligo is specific
			
			#flag
			my $good;
			foreach my $p(@found_pattern){
				$good=0;
				#remove taxon identifiers
				$p=~s/^.*?__//g;
				foreach my $c(@childs){
                                	if($c =~ m/$p/){
						$good=1;
                                	}
				}
				if(!$good){
                                	print BAD "Node has children:\n";
                                        print BAD join("\n",@childs);
                                        print BAD "\n";
                                        print BAD $cur_id,"\tReason-Sequence did not belong to the node:$p\n$cur_seq\n";
                                        return;
                                }
			}
			print GOOD $cur_id, "\t$cur_seq\n";
		}
		else{
			#if it is a leaf, and has more than 1 idenfication, then it is not specific
			print BAD $cur_id,"\tReason-Not specific for this sequence:\n";
			print BAD join("\n",@taxonomy),"\n";
		}
	}
	#oligo had no perfect match in blast results, so it is specific
	else{
		print GOOD $cur_id, "\t$cur_seq\n";
	}

	        
}
#get a node list for all superspecific sequences
sub cross_validate_nodelist{
	my $s_oligo=shift; 
        #read names file into hash
        my %names_hash;
	if($main::nameswitch){
        	open (NAMES,$main::namesfile) or die $!;
        	while(my $cur_line= <NAMES>){
                	chomp($cur_line);
                	my @line = split (/\t/,$cur_line);
                	my $original=$line[0];
                	my $identical=$line[1];
                	$names_hash{$original}=$identical;
       		}
        	close NAMES or die $!;

	}
	#get a list of uniq oligos
	#print "$s_oligo.superspecific.tab\n";
	open(SUPER, "<$s_oligo.superspecific.tab") or die $!;
	my @processed;
	foreach(<SUPER>){
		my @split=split(/-len[0-9]/,$_);
		#print "I'm here!\n";
		#print "$split[0]\n";
		push(@processed,$split[0]);
	
	}
	@processed= uniq(@processed);

	#`cut -d -len -f 1 $s_oligo.superspecific.tab | sort | uniq > uniq.oligo`;
	#open (OLIGO, "uniq.oligo") or die $!;
	#open necessary files
	my $treeio=Bio::TreeIO ->new(-format => "newick", -file => "$main::s_tree.nwk");
	my $treenwk = $treeio->next_tree;
	                                         
	open (OUT, ">$s_oligo.superspecific.descendents.tab"); #node list
	
	#go through the list of generated oligos
	foreach my $cur_oligo(@processed){
		chomp $cur_oligo;
                print OUT $cur_oligo,"\t";
                my $output;
		#if nodes then get all leaf nodes
                if($cur_oligo=~m/Node/){
                        my $node=$treenwk->find_node ( -id => $cur_oligo);
                        my @children=$node->get_all_Descendents();
                        foreach my $child (@children){
                                if($child->is_Leaf()){
					my $child_id=$child->id();
					my $value=($main::nameswitch)?$names_hash{$child_id}:$child_id;
					#if representative sequence found, then get all identical sequence
                                        if(defined $value){
                                                $output.="$value,";
                                        }
					else{
						$output.="$child_id,";
					}
                                }
                        }
                        chop($output);
                        print OUT $output, "\n";
                }
		#if leaf then print the identical sequences
                else{
			if($main::nameswitch){
                        	print OUT $names_hash{$cur_oligo},"\n";
			}
			else{
				print OUT $cur_oligo, "\n";
			}
                }
	}

	#unlink "uniq.oligo";
	#close OLIGO or die $!;
	close OUT or die $!;
	close SUPER or die $!;
}
sub combine_all{
	my $good_file = shift; # oligos that passes the crosscheck, normally $s_oligo.superspecific.tab
        my $bad_file = shift;  # oligos that didn't pass, normally notspecific.seq

        # directory structure
        my $good_dir = "tmp/good";
        my $bad_dir = "tmp/bad";

	opendir (DIR, $good_dir);
	open (ALL_GOOD,">$good_file")or die;

	#write all sequences not in blast results
	open(NOT_IN_BLAST, "tmp/not_in_blast.tmp")or die;
	print ALL_GOOD <NOT_IN_BLAST>;
	close NOT_IN_BLAST;
	#delete unnecessary files
	unlink ("tmp/not_in_blast.tmp");
        unlink("tmp/unique.tmp");
	#write all sequences that passes the cross validation
	while (readdir (DIR)){
		my $currfile = $_;
		open (CURR,"$good_dir/$currfile")or die;
		print ALL_GOOD <CURR>;
		close CURR;
		#delete unnecessary files
		unlink ("$good_dir/$currfile");
	}
	close ALL_GOOD or die $!;
        closedir(DIR);

        # write all sequences that didn't pass
        opendir(DIR, $bad_dir);
        open(ALL_BAD, ">$bad_file") or die;
        
        while (readdir(DIR)) {
          my $currfile = $_;
          open(CURR, "$bad_dir/$currfile") or die;
          print ALL_BAD <CURR>;
          close CURR;
          unlink("$bad_dir/$currfile");
        }
        close ALL_BAD or die $!;
        closedir(DIR);
}


sub fasta_to_OFP{
	#simple subroutine that makes fasta files into OFP style format
	my $in_file = shift;
	my $s_file = basename ($in_file,".fasta",".tab","tab"); #takes out .fasta in input name
	open (IN,$in_file) or die;
	open (OUT,">$s_file.fasta")or die;
	while (<IN>){
		#converts the OFP format into fasta
		s/^/>/g; #replaces start line with >
		s/\t/\n/g; #replaces tabs with newline
		print OUT $_;
	}
	close IN;
	close OUT;
}

sub fasta_to_gff3{
	#subroutine that converts the fasta oligos to gff3 format.
	#seqid src type start end score strnad phase attr
	#the oligo name is separated by '-', the last of which contains the start and end
	#attr column will have ID 
	my $in_file = shift;
	my $orig_fasta = shift; #original input fasta to be put at end

	open (FASTAFILE,$in_file)or die;
	my $s_file = basename ($in_file,".fasta",".tab","tab"); #takes out .fasta in input name
	open (NEWFILE,">$s_file.gff")or die;
	my @fasta = <FASTAFILE>; #reads the fasta into memory as it will need to be placed later inside
	close FASTAFILE;

	print NEWFILE "##gff-version 3\n";

	my $count=0; #count to generate unique ids
	foreach (@fasta){
		#goes through the fasta, split the needed info
		my $line = $_;
		if ($line =~ m/>/){ #if line matches and ID
			#split the needed info
			$line =~ s/>//;#takes out the >
			my @fields = split(/\-/,$line);
			my $bounds = $fields[-1]; #start and end
			my $seqid = $fields[0];
			my $type = $fields[1];
			#the start and end fields need to be split into actual integers
			$bounds =~ s/\(//;
			$bounds =~ s/\)//;

			my @startend = split (/e/,$bounds);
			my $start = $startend[0];#this still has the s, so we need to take it out
			$start =~ s/s//;
			my $end = $startend[1];
			chomp $start;
			chomp $end;

			$count++;
			
			#values are more or less here, we can print the gff style			
			print NEWFILE "$seqid\t.\t$type\t$start\t$end\t.\t+\t.\tID=$seqid-$count;Name:";
		}else{
			print NEWFILE $line;
		}	
	}
	#print the original fasta file to the end
	print NEWFILE "##FASTA\n";
	open (ORIG,$orig_fasta) or die;
	print NEWFILE <ORIG>;
	close NEWFILE;
}
sub color_oligo{

	#open node list file and get the ids
	my $node_list=shift;
	open( FH, "<$node_list");
	my @nodes;
	#get ids
	while (<FH>) {
    		chomp $_;
    		my @split=split("\t",$_);
    		push @nodes, $split[0];
	}
	close FH;
	my $treeio=Bio::TreeIO ->new(-format => "newick", -file => "$main::s_tree.nwk");
	my $tree1 = $treeio->next_tree;

	#find id in the tree and color it red
	foreach my $n (@nodes) {
    		#print $n, "\n";
    		my $node = $tree1->find_node( -id => "$n" );
    		#print $node->id, "\n";
    		if(!defined $node){
			warn "$node is not found in the tree, there is something wrong with your input tree file. Please double check your input files for special characters (/,*,-)";
			next;
    		}
    		if($node->is_Leaf()){
         		$node->add_tag_value( 'Rcolor', 1 );
    		}
    		else{
    			$node->add_tag_value( 'Rcolor', 1 );
    			foreach my $child ( $node->get_all_Descendents() ) {
        			#print "Child",$child->id, "\n";
        			$child->add_tag_value( 'Rcolor', 1 );
    			}
    		}
	}
	#output the new tree to cladogram
	my $cg = Bio::Tree::Draw::Cladogram->new(

   	 -tree   => $tree1,
   	 -colors => 1,
   	 -bootstrap => 1
	);

	$cg->print( -file => "$main::s_tree.witholigo.eps" );    
}
#get patterns from oligos
sub get_patterns_from_oligos{
	my $s_oligo=shift;
	open(OLIGOS, "<$s_oligo.tab") or die $!;
        my @processed;
	my @patterns;
        foreach(<OLIGOS>){
                my @tab_split=split(/-len[0-9]/,$_);
                push(@processed,$tab_split[0]);
        }
	@processed= sort (@processed);
        @processed= uniq(@processed);
	foreach my $oligo (@processed){
		if($oligo !~m/Node/){
                	#print "$oligo\n";
                	my @oligo_split=split(/_/,$oligo);
			#push genus name, species name
			push(@patterns,join("_",$oligo_split[2],$oligo_split[3]));
                }
	}
	@patterns=sort(@patterns);
	@patterns=uniq(@patterns);
	close OLIGOS or die $!;
	#print join("|",@patterns),"\n";
	return join("|",@patterns);

}
1;
