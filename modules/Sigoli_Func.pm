#! /bin/usr/env perl

package Sigoli_Func;
# Part of the AODP pipeline.
# Contains subroutines that deal with running sigoli and/or working with sigoli outputs.
# This module specifically requires the program sigoli - the AODP pipeline will check whether it is installed.

#MIT License
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Sigoli: SigOli is now distributed under the GNU General Public License (GPL).

#DOI: 10.5281/zenodo.10762

# authors: Kaisheng (Kevin) Chen, Chuyang (Charles) Qi, Wen Chen* (wen.chen@agr.gc.ca)

use strict;
use threads;
use Thread::Queue;
use threads::shared;
use File::Spec;
use Bio::SeqIO;
use Bio::Seq;
use File::Find;
use File::Basename;
use Cwd; #use current directory
use Time::HiRes qw(usleep nanosleep);	
use POSIX qw/strftime/;
use File::Glob;
use Getopt::Long;


#call_sigoli needs these variables
my $sdir;
my @sfolders;
my $progresstotal;
my $pcount : shared;

#Percentage calculations for specific and OFP
my $total_sofp;
my $sofp : shared;
$sofp = 0;


sub start_sigoli{
	#Prepares to call sigoli by getting needed folder data. 
	my $thread=shift;
	my $min_length=shift;
	my $max_length=shift;
	my $fasta_file=shift;
	my $dir=shift;
	$sdir = $dir; #sets the global variable to be equal to local
	my $the_cwd=cwd(); #uses cwd module to get the current directory

	print ("Sigoli is running:\n");

	##############################################
	opendir(DIR, $dir) or die $!; #opens tree_and_nodes

	my @sub_folders; #array of all the nodes inside tree and nodes folder
	while (my $sub_folder = readdir(DIR)) { #goes through the dir
		# Use a regular expression to ignore files beginning with a period
		if ($sub_folder =~ m/^Node/) {
			push @sub_folders, "$sub_folder\n"; #pushes the proper subfolder onto the array
		}
	}
	closedir(DIR);#closes tree_and_nodes  
	#sorts the sub_folder array
	@sub_folders = sort @sub_folders;

	#calculate totals
	$pcount = 0;
	$progresstotal = (scalar @sub_folders)*(($max_length+1) - $min_length);

	my $num=scalar(@sub_folders); #stores number of elements in sub_folders
	@sfolders = @sub_folders; #sets the global variable for sigoli


###################################################
	my @range; #array of the range of oligos
	for (my $i=$min_length; $i<=$max_length; $i++) {
		push @range, $i;
	}


# THREADED PORTION 
# Create a queue to push work onto and the threads to pull work from
# Populate it with all the data up front so threads can finish when
# the queue is exhausted.  Makes things simpler.
# See https://rt.cpan.org/Ticket/Display.html?id=79733
# Each oligo length will be assigned to a node if possible. This means that each thread will be 
# processing all the folders and looking for the specified length.
	my $queue = Thread::Queue->new(@range);
	#print $queue, "\n";

	# Create a bunch of threads to do the work
	my @threads;
	for(1 .. $thread) { 
		push @threads, threads->new( sub {
				# Pull work from the queue, don't wait if its empty
				while( my $lines = $queue->dequeue_nb ) {
					# Do the work
					$lines=~ s/ //g;
					chomp $lines; #eliminates newline
					call_sigoli($fasta_file, $lines); #calls sigoli
				}
			}); 
	}

	# Wait for all threads to finish 
	$_->join for @threads;
	printf ("100.0%%\r");

}

#diff to NO 
####################################################################
sub call_sigoli {
# Subroutine that actually calls the sigoli program
# Make sure sigoli is actually installed and runs on the command line
  my $arg_fasta=shift or die "no oligo_file name";
  my $arg_len=shift or die "no directories to process";
  my $basename=basename ($arg_fasta, ".fasta");
	#runs for every found folder	
	foreach my $sub_dir (@sfolders) {
		chomp $sub_dir;
		$sub_dir=~ s/ //g;
		if ($sub_dir=~ m/Node/) {
			#sigoli call from system
			#ambiguous option is obsolete, it will always consider ambiguous
			if	(system ("sigoli -operation=strings -oligo-size=$arg_len -sequence-directory=$sub_dir -diff=no crowded=no -first-site-gap=5 -inter-site-gap=8 -ambiguous=yes >> $sdir\/sigoli_$basename.strings.len$arg_len.txt") != 0 ){exit("Sigoli went wrong\n");} # outputs in the same directory as tree_and_nodes, moved later 
	

			$pcount ++;
			printf ("%.1f%%\r",($pcount/$progresstotal)*100); #prints out progress
		}
	}
}

################################################################

sub specific_OFP_gen {
	#sigoli.$filename folder
	my $oligo_min = shift;
	my $oligo_max = shift;
	my $s_filename = shift;
	my $thread = shift;
	#get total steps for ofp and specificity
	print ("Creating specific and non specific files and OFP files:\n");
	#=pod
	my @range;
	for (my $i = $oligo_min;$i<=$oligo_max;$i++){
		open (COUNTER,"sigoli.$s_filename/sigoli_$s_filename.strings.len$i.txt") or die;
		while (<COUNTER>){}
		$total_sofp += $.;
		close COUNTER;
		push (@range, $i);
	}  
	#=cut
	my $queue = Thread::Queue->new(@range);
	my @threads;
	for (1 .. $thread){
		push @threads, threads->new (sub {
				# Pull work from the queue, don't wait if its empty
				while( my $lines = $queue->dequeue_nb ) {
					# Do the work
					$lines=~ s/ //g;
					chomp $lines; #eliminates newline
					specificity_check("sigoli.$s_filename/sigoli_$s_filename.strings.len$lines.txt","$s_filename.notaln.fasta");
					#This sub attempts to fix 454pyroseequencing error, and outputs OFP files.
					OFP_mod("sigoli.$s_filename/sigoli_$s_filename.strings.len$lines.txt.oligo.specific",$lines);

				}
			}); 
	}
	$_->join for @threads;
	print ("100.0%\r");

}
sub specificity_check{
	#checks the specificty of sigoli results, and outputs specific and non specific files found.
	#Grabs the inputs
	my $sigoli_file=shift or die "$!";
	my $fasta=shift or die "$!";
	my $line;
	my $file;
	my $count;
	#opens sigoli output files previously generated
	open(my $fh_sigoli, "<$sigoli_file") or die "Sigoli string output file?";

	open (my $O_fh, ">$sigoli_file.withcluster") or die "Cannot open $sigoli_file.withcluster";

	#opens fasta file
	open (FH, $fasta);
	my @fasta_array=<FH>; #reads the fasta into memory!
	chomp @fasta_array;

	my $count1; #count to keep track of lines
	my $clade; 
	#tranverses the sigoli file
	while(<$fh_sigoli>) {
		$sofp ++;
		printf ("%.1f%%\r",($sofp/$total_sofp)*100);

		$line=$_;
		chomp $line;
		next if ($line=~ m/^---/ || $line=~ /^$/);
		#matches beginning or nothing

		if ($line=~ m/tree_and_nodes/ ) {
			$clade=$line;
			print $O_fh "\n\n",$line,"\n";
			my $dirname=dirname($line);
			opendir(DIR, "$dirname") or die "Cannot open $dirname\n";
			my @FILES= readdir(DIR); 
			$count=0;
			foreach $file (@FILES) {
				$count++;
				print $O_fh $file, "\n";
			}
			$count1=$count-2;			
			print $O_fh "Total # of sequences in $dirname is: $count1", "\n\n";

		} else {
			my $count2=scalar ( grep( /$line/, @fasta_array ));
			my @seq = (grep(/$line/, @fasta_array));

			my $index=-1;
			if (defined $seq[0]){
				$index = index($seq[0],$line);
			}
			#get the pos of the oligo
			print $O_fh "oligo-",$clade,"(s",($index+1),"e",(length $line)+$index,")\t",$line,"\t", $count1,"\t", $count2, "\n";

			next;
		}

	}
	close $fh_sigoli;
	close $O_fh;

	#opens the new outputs
	open(my $fh_cluster, "<$sigoli_file.withcluster") or die "$!";
	open (my $o_specific, ">$sigoli_file.oligo.specific") or die "$!";
	open (my $o_specific1, ">$sigoli_file.oligoOnly.specific") or die "$!";
	open (my $o_notspecific, ">$sigoli_file.oligo.notspecific") or die "$!";
	open (my $o_notspecific1, ">$sigoli_file.oligoOnly.notspecific") or die "$!";

	while (<$fh_cluster>) {
		$line=$_;
		chomp $line;
		if ($line=~ m/oligo-/) {
			my @fields=split "\t", $line;
			my $oligoid=$fields[0];
			my $oligo=$fields[2];
			my $node_seq_num=$fields[3];
			my $match_in_fasta=$fields[4];

			if ($match_in_fasta == $node_seq_num) {
				print $o_specific $line, "\n";
				print $o_specific1 $oligo, "\n";
			} else {
				print $o_notspecific $line, "\n";
				print $o_notspecific1 $oligo, "\n";
			}
		}
	}

	close $fh_cluster;
	close $o_specific;
	close $o_specific1;
	close $o_notspecific;
	close $o_notspecific1;

}
#########################################################################################
sub OFP_mod{
 #fixes 454 pyrosequencing error and outputs tab files
 #these OFP are to be used in the OFP pipeline and adhere to a different style than the standard fasta
 
 my $sigoli_file=shift or die "$!";
 my $oligo_len=shift or die "$!";
 my $filename= basename("$sigoli_file", ".strings.len$oligo_len.txt");
 $filename=~ s/sigoli_//g;
 my $line;
 my $file;
 my $count;

 open(my $fh_sigoli, "<$sigoli_file") or die "Sigoli string output file with only specific oligos?";

 open (my $O_fh1, ">$sigoli_file.withLonghomo.tab.oldnodeformat") or die "Cannot open $sigoli_file.withLonghomo.tab.all";
 open (my $O_fh2, ">$sigoli_file.withLonghomo.tab") or die "Cannot open $sigoli_file.withLonghomo.tab.specific";
 my $nodes;
 $nodes={};

 my $node_old;
 my $node_new;
 #count the occurance
 $count=0;
 while(my $line =<$fh_sigoli>) {
  chomp $line;
  my @fields1= split /\t/, $line;
  my $node1=$fields1[0];
  $node1=~ s/oligo-tree_and_nodes\///g;
  my @fields2=split /\//, $node1;
  my @fields3=split /\./, $fields2[1];
  my $node2=$fields3[0];
  $node_old=join("_", $fields2[0], $node2);
  $node_new=$node2;	
  $count++;
	#print the sequence separated by TAB
  print $O_fh1 $node_old,"-", "len$oligo_len-$fields1[1]","\t", $fields1[2], "\n";
  print $O_fh2 $node_new,"-", "len$oligo_len-$fields1[1]","\t", $fields1[2], "\n";
 }
 close $fh_sigoli;
 close $O_fh1;
 close $O_fh2;
}
###########################################################################################
sub OFP_multilen {
	#Reads in hash tables, outputs $file.max4homo.tab and $file.wLhomo.tab
	#essentially eliminates long homogeneous bases
	my $sigoli_file=shift or die "$!";
	my $filename= basename("$sigoli_file", ".tab");
	#subroutine: read in hash tables
	sub read_hash {
		my $fname = shift;
		open (my $fh, "<",$fname) or die "$!";  
		my %hash=();
		while (<$fh>) {
			chomp;
			my ($key,$value)=split /\t/;
			$hash{$key}=$value;
		}
		return \%hash;
	}

#open (my $O_fh1, ">$filename.wLhomo.tab") or die "Cannot open $filename.wLhomo.tab";
open (my $O_fh2, ">$filename.max4homo.tab") or die "Cannot open $filename.max4homo.tab";

my $h_sigoli=read_hash("$sigoli_file");
my $min=5; # minimum homopolymer # 5
#my $count1=0;
#my %seen1;
#foreach my $oligo_name1 (sort keys $h_sigoli) {
#	#	print $oligo_name1, "\n";
#	my @fields1=split /-/, $oligo_name1;
#	my $node1=join("-",$fields1[0], $fields1[1]);
#	#	print $node1, "\n";
#	my $oligo1=$h_sigoli->{$oligo_name1};
#	$count1++;
#	#	print $node1, "-", $count1,"\t", $oligo1, "\n";
#	print $O_fh1 $node1, "-", $count1,"\t", $oligo1, "\n";
#
#}

foreach my $oligo_name2 (sort keys $h_sigoli) {
	my $oligo2=$h_sigoli->{$oligo_name2};

        # discard if oligo has a homopolymer above minimum length
	if ($oligo2 =~ /(A{$min,}|T{$min,}|G{$min,}|C{$min,})/g) {
		next;
	} else {
		print $O_fh2 $oligo_name2,"\t", $oligo2, "\n";

	}
}	
	#close $O_fh1;
	close $O_fh2;

}

1;
