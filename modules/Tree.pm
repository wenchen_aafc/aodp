#! /bin/usr/perl
package Tree;
use Bio::TreeIO;
use Bio::Tree::NodeI;
use Bio::SeqIO;
use File::Find;

use threads;
use Thread::Queue;
use threads::shared;

#Part of AODP
#Contains subroutines to work with trees_and_nodes folder
#treeprep creates the folders and files for the other functions to work on
#createnodes generates the node folders inside tree and nodes, and puts the seqid files inside.

#MIT License
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#Sigoli: SigOli is now distributed under the GNU General Public License (GPL).

#DOI: 10.5281/zenodo.10762

## authors: Kaisheng (Kevin) Chen, Chuyang (Charles) Qi, Wen Chen* (wen.chen@agr.gc.ca)

use warnings;
use strict;

#variables used to calculate progress
my $nodetotal=0;
my $leaftotal=0;

sub treeprep{
 #prepares the enviroment for sigoli by creating the folder and node list
 #takes no parameters
 #generates Nodes.list in working directory
 #generates tree_and_nodes folder in working directory
 my $nwktree = shift or die ("Provide nwk file");
 
 #Searches in_tree.nwk to find the node numbers with a grep command, and creates Nodes.list
 open (NWK,$nwktree);
 open (NOUT,">Nodes.list");
 while (<NWK>){
  my @temp = $_ =~ m/(Node[0-9]*)/g; #matches for lines that have the node, grab only that part
  foreach (@temp){
   print NOUT $_,"\n"; #adds a newline to the file
   $nodetotal++;
  }
 }
 close NWK;
 close NOUT;

 #Checks if the tree_and_nodes folder already exists. If it does, delete it
 if (-e "tree_and_nodes"){
	  rmtree ("tree_and_nodes") or die ("Could not remove tree_and_nodes!\n");                                  #force removes the folder if it exits already
 }else{
  mkdir ("tree_and_nodes");                                                                                        #doesn't exist, create the folder
  print ("Created tree_and_nodes...\n");
 }
 
}

############################################################################################

sub createnodes{
 #creates trees and nodes subfolders inside the folder
 
 print ("Creating subfolders and leaves in tree_and_nodes:\n"); 
 my ($treefile, $nodeid)=@_; #grabs the input from the array

 my $treeio=Bio::TreeIO ->new(-format => "newick", -file => $treefile);
 #my $treeout=Bio::TreeIO ->new(-format => "newick", -file => ">$treefile.nwk");

 my $tree = $treeio->next_tree; 


 my $rootnode = $tree->get_root_node;
 my $rootnodeid=$rootnode->id;

 # process just the all generations
 open (OUT, ">$rootnodeid.id");
 foreach my $root_childs ($rootnode->get_all_Descendents()){
	my $rootchildsid = $root_childs->id;
	unless ($rootchildsid=~ /Node/) {
		$rootchildsid=~ s/'//g;
		print OUT $rootchildsid,"\n";
		}
 }		
		 

 open (FH, "<", $nodeid) or die $!;
 my $nodecount =1;
 while ( my $line=<FH>) {
        printf ("%.1f%%\r",($nodecount/$nodetotal)*100);  
        $nodecount++;
	chomp $line;
	mkdir $line;	
	my $current_node= $tree -> find_node(-id=> $line);
	# process just the next generation
	unless ($line eq "Node1") {
		#gets all descendants for the node
		open (OUT1, ">$line/$line") or die $!;
		foreach my $node_all ($current_node->get_all_Descendents() ) {
			my $node_allid=$node_all->id;
			$node_allid=~ s/'//g;
			unless ($node_allid=~ /Node/) {
			$node_allid=~ s/'//g;
			print OUT1 $node_allid,"\n";
			}
		}
		foreach my $node ($current_node->each_Descendent() ) {
			my $nodeid=$node->id;
			$nodeid=~ s/'//g;
			open (OUT2, ">$line/$nodeid.seqid") or die $!;
			$leaftotal++;
			if ($node->is_Leaf){
				$nodeid=~ s/'//g;
				print OUT2 $nodeid;
			} else {		
				foreach my $child( $node->get_all_Descendents()) {
					my $id =$child->id;
					unless ($id=~ /Node/) {
					$id=~ s/'//g;
					print OUT2 $id,"\n";
					}					
				}
			}
		}
	
		my $other="other.seqid";
		open (OUT3, ">$line/$other");
  		$leaftotal++;
		my @other_list=`grep -vFif $line/$line $rootnodeid.id`;
		print OUT3 @other_list;	
		my $del="$line/$line";
		unlink $del or die;
	}
	
	if ($line eq "Node1") {
		mkdir "Node1";
		foreach my $root_node ($current_node->each_Descendent() ) {
			my $root_decen=$root_node->id;
			$root_decen=~ s/'//g;
			open (OUT4, ">Node1/$root_decen.seqid");
 			$leaftotal++;
			if ($root_node->is_Leaf){
				$root_decen=~ s/'//g;
				print OUT4 $root_decen;
			} else {		
				foreach my $root_child( $root_node->get_all_Descendents()) {
					my $root_child_id =$root_child->id;
					unless ($root_child_id=~ /Node/) {
					$root_child_id=~ s/'//g;
					print OUT4 $root_child_id,"\n";
					}					
				}
			}
		}
		
	
	}	
 }
 close OUT;
 close OUT1;
 close OUT2;
 close OUT3;
 close OUT4;
 close FH;
 print "\n";
}

##############################################################################################
my $digcount :shared;
$digcount =0;
my @allseqs; #list of allseq files found for multithread (full path relative)

sub tranverse { #needs $filename.notaln.fasta for seqid_extract
	print ("Traversing the tree:\n");
	my $dir = "tree_and_nodes";
	my $notaln = shift;
	my $thread = shift;
	$notaln = "$notaln.notaln.fasta";

	find ({wanted => sub {&dig($notaln);}, no_chdir => 1},$dir);  
	#gets all the seqs in @allseqs.
	my $queue = Thread::Queue->new(@allseqs);
	my @threads;
	for (1 .. $thread){
		push @threads, threads->new (sub {
				# Pull work from the queue, don't wait if its empty
				while( my $lines = $queue->dequeue_nb ) {
					# Do the work
					$lines=~ s/ //g;
					chomp $lines; #eliminates newline
					&explode ($notaln,$lines);
				}
			}); 
	}
	$_->join for @threads;
	printf ("100.0%%\r");
}

sub explode{
	my $notaln = shift;
	my $found = shift;
	open my $seqhandle, ">$found.allseq.fasta" or die;
	&seqid_extract($found, $notaln, $seqhandle);
	&parse_seqs("$found.allseq");
	$digcount++;
	printf ("%.1f%%\r",($digcount/$leaftotal)*100);
	unlink $found or die;
	unlink ("$found.allseq.fasta") or die;
}

sub dig {
 my $notaln = shift;
 my $found = $_;
 if ($found =~ m/seqid/){
  push (@allseqs,$found);
 }
}

###########################################################################################

sub seqid_extract{
 #Extracts a list of sequences based on their seqID
 #Creates .allseq.fasta files in corresponding
 #Requires 3 parameters to function: ID value, fasta file name, and the filehandle of the output file
 #Local variables as parameters
 my $id = $_[0];                                                         #the id value that was found
 my $fasta = $_[1];                                          #the not aligned fasta file from script1
 my $fhandle = $_[2];                                       #the file that it is being printed out to
 my %ids;                                                 #hash that records values as they are found
 open (ID,"$id");                                                  #opens the file found in the nodes

 while (<ID>){
  #goes through the file and grabs the string
  #these regular expressions splice the string
  s/\r?\n//;
  /^>?(\S+)/;
  $ids{$1} ++;                                        #increment the corresponding string in the hash
 }

 my $num_ids = keys %ids;

 open(F, $fasta) or die;                                                          #opens the unaligned fasta
 #booleans to decide whether to print
 my $s_read = 0;                                                              #counter for lines read
 my $s_wrote = 0;                                                          #counter for lines written
 my $print_it = 0;                                          #tells the script whether to print or not

 while (<F>){                                                              #transcends the fasta file
   if (/^>(\S+)/){                                                     #regular expression matching S
    $s_read++;                                                              #increments the read line
     if ($ids{$1}){
      $s_wrote++;
      $print_it = 1;                                                                  #print the line
      delete $ids{$1};
     }else{
      $print_it = 0;                                                            #don't print the line
     }
   }
   if ($print_it) {
    print $fhandle $_;                                               #prints the sequence to the file
   }
 }
close F;
close ID;
close $fhandle;

}

###################################################################################################

sub parse_seqs{
 my $fasta=$_[0] or die ("Please provide an input fasta file containing multiple sequences");
 my $seqio=Bio::SeqIO->new(-format=> "fasta",
				 -file => "$fasta.fasta") || die ("Unable to create SeqIO object from $fasta");
 mkdir $fasta or die "couldn't make: $!";

 while(my $seq=$seqio->next_seq()){
	my $filename=$seq->id;
	open (AND, ">", "$fasta/$filename.fasta") or die "couldn't open: $!";

	print AND ">","$fasta/",$seq->id, "\t", $seq->desc, "\n", $seq->seq;
	close AND;
  }
}

1;
################################################################################

__END__

=head1 NAME

Tree Generation for AODP

=head1 SYNOPSIS

	use Tree.pm

	treeprep("NWK");
	createnodes("NWK","LST");
	tranverse("FILE","THREAD");

=head1 DESCRIPTION

Contains functions that work with the input tree file from AODP. Treeprep checks the input files for correctness, while createnodes generates the files and tranverse goes through and parses the sequences out from the input fasta. 





